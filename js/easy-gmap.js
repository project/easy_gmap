/**
 * Google Maps API reference can be found here:
 * https://developers.google.com/maps/documentation/javascript/reference
 *
 */
(function($) {

  Drupal.easyGmap = Drupal.easyGmap || {};
  var timeoutId;

  var $addressLoading = $(
    '<div id="eg-address-loading" style="display:inline-block;">' +
      '<span class="easy-gmap-spinner"></span>' +
      Drupal.t('Loading address...', {}, {context: 'easy_gmap'}) +
    '</div>'
  );
  var $addressError = $(
    '<div id="address-error" style="display:inline-block;background-color:#ff898e;padding:3px;">' +
      Drupal.t('Address not found.', {}, {context: 'easy_gmap'}) +
    '</div>'
  );

  Drupal.behaviors.easyGmap = {
    attach: function (context, settings) {
      Drupal.easyGmap.init();
    }
  };

  /**
   * Initialize variables and the map.
   */
  Drupal.easyGmap.init = function() {
    Drupal.easyGmap.address = $("input[name='" + Drupal.settings.easyGmap.field + "[address]']");
    Drupal.easyGmap.latitude = $("input[name='" + Drupal.settings.easyGmap.field + "[latitude]']");
    Drupal.easyGmap.longitude = $("input[name='" + Drupal.settings.easyGmap.field + "[longitude]']");
    Drupal.easyGmap.zoom = $("input[name='" + Drupal.settings.easyGmap.field + "[zoom]']");
    $(".easy-gmap-edit").each(Drupal.easyGmap.initEditableMap);
  }

  /**
   * Initialize editable maps
   */
  Drupal.easyGmap.initEditableMap = function() {
    var zoom, options;
    Drupal.easyGmap.coder = new google.maps.Geocoder();
    zoom = Drupal.easyGmap.zoom.val();
    zoom = parseInt(zoom);
    if (isNaN(zoom)) {
      zoom = 8;
    }
    options = {zoom: zoom};

    // Render the map.
    Drupal.easyGmap.map = new google.maps.Map(this, options);

    //Set the marker on address update.
    Drupal.easyGmap.address.keyup(function (e) {
      Drupal.easyGmap.addMarkerByAddress(e.target.value, 2000);
    });

    //Set the marker on click and update the address field.
    Drupal.easyGmap.map.addListener('click', function (event) {
      Drupal.easyGmap.addMarkerByCoordinates(Drupal.settings.easyGmap.map, event.latLng, true);
    });

    // Save field values on submit.
    $('form').submit(function (e) {
      var position, zoom;
      position = Drupal.easyGmap.marker.getPosition();
      Drupal.easyGmap.longitude.attr('value', position.lng());
      Drupal.easyGmap.latitude.attr('value', position.lat());
      zoom = Drupal.easyGmap.map.getZoom();
      if (!isNaN(zoom))
        Drupal.easyGmap.zoom.attr('value', zoom);
    });

    // Add marker based on the address field value.
    Drupal.easyGmap.addMarkerByAddress(Drupal.easyGmap.address.val(), 0);
  }

  Drupal.easyGmap.addMarkerByAddress = function(address, delay) {
    clearTimeout(timeoutId);

    $addressError.detach();
    if(!$.contains(document, $addressLoading[0])) {
      Drupal.easyGmap.address.after($addressLoading);
    }
    timeoutId = setTimeout(function() {
      $addressLoading.detach();
      if(address) {
        Drupal.easyGmap.coder.geocode({'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            Drupal.easyGmap.removeMarker();
            Drupal.easyGmap.map.setCenter(results[0].geometry.location);
            Drupal.easyGmap.addMarker(Drupal.easyGmap.map, results[0].geometry.location, false);
          } else {
            Drupal.easyGmap.address.after($addressError);
          }
        });
      }
    }, delay);
  }

  Drupal.easyGmap.addMarkerByCoordinates = function(lat, lng) {
    if (lng && lat) {
      Drupal.easyGmap.marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: Drupal.simpleGmap.map
      });
    }
  }

  /**
   * Add marker to the given map on the given point
   */
  Drupal.easyGmap.addMarker = function(map, point, updateAddress) {
    updateAddress = !!updateAddress;

    if (Drupal.easyGmap.marker != null) {
      Drupal.easyGmap.marker.setMap(null);
    }

    Drupal.easyGmap.marker = new google.maps.Marker({
      position: point,
      map: map
    });

    if(updateAddress) {
      Drupal.easyGmap.coder.geocode(
        {'latLng': Drupal.easyGmap.marker.getPosition()},
        function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            Drupal.easyGmap.map.setCenter(results[0].geometry.location);
            Drupal.easyGmap.address.attr('value', results[0].formatted_address);
          } else {
            alert(Drupal.t('Getting address for this point failed. Error: ' + status, {}, {context: 'easy_gmap'}));
          }
        }
      );
    }
  }

  Drupal.easyGmap.removeMarker = function() {
    if (Drupal.easyGmap.marker) {
      Drupal.easyGmap.marker.setMap(null);
      Drupal.easyGmap.marker = null;
    }
    return false;
  }

})(jQuery);
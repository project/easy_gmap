<?php

/**
 * Administration form.
 */
function easy_gmap_admin_form($form) {
  $form = array();
  $form['easy_gmap_api_key'] = array(
    '#title' => t('Google Maps API Key'),
    '#description' => t('Enter the Google Maps API key.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('easy_gmap_api_key', ''),
  );
  return system_settings_form($form);
}


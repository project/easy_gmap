<?php
/**
 * @file
 * Displays the easy Google Maps formatter.
 *
 * Available variables:
 * - $include_map: TRUE if an embedded dynamic map should be displayed.
 * - $include_static_map: TRUE if an embedded static map should be displayed.
 * - $width: Width of embedded map.
 * - $height: Height of embedded map.
 * - $include_link: TRUE if a link to a map should be displayed.
 * - $link_text: Text of link to display.
 * - $url_suffix: Suffix of URLs to send to Google Maps for embedded and linked
 *   maps. Contains the URL-encoded address.
 * - $zoom: Zoom level for embedded and linked maps.
 * - $information_bubble: TRUE if an information bubble should be displayed on
 *   maps. Note that in the Google Maps URLs, you need to send iwloc=A to use
 *   a bubble, and iwloc=near to omit the bubble.
 * - $address_text: Address text to display (empty if it should not be
 *   displayed).
 * - $map_type: Type of map to use (Google code, such as 'm' or 't').
 * - $langcode: Two-letter language code to use.
 * - $static_map_type: Type of map to use for static map (Google code, such as
 *  'roadmap' or 'satellite')
 *
 * @ingroup themeable
 */

$static_w = (int) $width;
$static_h = (int) $height;
$q = $url_address;
?>
<?php if(!empty($address_text)): ?>
  <p class="easy-gmap-address"><?php print $address_text; ?></p>
<?php endif; ?>
<?php if($include_map): ?>
<iframe width="<?php print $width; ?>" height="<?php print $height; ?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?language=<?php print $langcode; ?>&amp;q=<?php print $q; ?>&amp;zoom=<?php print $zoom; ?>&amp;key=<?php print $api_key; ?>"></iframe>
<?php endif; ?>

<?php if ($include_link): ?>
  <p class="easy-gmap-link"><a href="//maps.google.com/maps?q=<?php print $q; ?>&amp;hl=<?php print $langcode; ?>&amp;zoom=<?php print $zoom; ?>&amp;t=m" target="_blank"><?php print $link_text; ?></a></p>
<?php endif; ?>